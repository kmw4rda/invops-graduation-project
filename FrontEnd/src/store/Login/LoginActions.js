import {SUCCESS_LOGIN} from './LoginActionTypes'
import {LOG_OUT} from './LoginActionTypes'

export const loggingIn = (data)=>{
    return{
        type:SUCCESS_LOGIN,
        userData:data
    }
}
export const logOut = ()=>{
    console.log()
    return{
        type:LOG_OUT,
    }
}
