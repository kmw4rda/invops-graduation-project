import Cookies from 'js-cookie'
import {LOG_OUT, SUCCESS_LOGIN} from './LoginActionTypes'

let initialState={
    user:Cookies.get('user')?JSON.parse(Cookies.get('user')): {},
    token:Cookies.get('token')?Cookies.get('token'): null,
    permissions:[],
    isLogged:Cookies.get('token')?true:false
}

const LoginReducer = (state=initialState,action)=>{
    switch (action.type){
        case SUCCESS_LOGIN:
            return{
                ...state,
                token:action.userData.token,
                permissions:action.userData.permission,
                isLogged:true,
                user:action.userData.data
            }
        case LOG_OUT:
            return{
                ...state,
                token:null,
                permissions:[],
                isLogged:false,
                user:{}

        }
        default: return state;
    }
}
export default LoginReducer