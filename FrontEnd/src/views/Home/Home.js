import React from 'react'
import PageBreadCrumb from 'components/Global/Layout/PageBreadCrumb/PageBreadCrumb'
import HomeHeader from 'components/Home/HomeHeader/HomeHeader'
import Courses from 'components/Home/Courses/Courses'
import GetStarted from 'components/Home/GetStarted/GetStarted'
import VideoSection from 'components/Home/VideoSection/VideoSection'
import Footer from 'components/Global/Layout/Footer/Footer'
import FAQs from 'components/Home/FAQs/FAQs'
import WordsFromStudents from 'components/Home/WordsFromStudents/WordsFromStudents'

function Home() {
  return (
    <>
      {/* <PageBreadCrumb/> */}
      <HomeHeader/>
      <Courses/>

      <GetStarted/>
      <VideoSection/>
      <WordsFromStudents/>
      <FAQs/>
      <Footer/>
    </>
  )
}

export default Home